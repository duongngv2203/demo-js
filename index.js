import Handsontable from "handsontable";
import 'handsontable/dist/handsontable.full.min.css';


const container = document.querySelector('#example');
const autosave = document.querySelector('#autosave');
const load = document.querySelector('#load');
const save = document.querySelector('#save');
const exampleConsole = document.querySelector('#output');

const hot = new Handsontable(container, {
    startRows: 8,
    startCols: 6,
    colHeaders: true,
    rowHeaders: true,
    height: 'auto',
    width: 'auto',
    licenseKey: 'non-commercial-and-evaluation',
    afterChange: function (change, source) {
        if (source === 'loadData') {
            return; 
        }
        if (!autosave.checked) {
            return;
        }

        fetch('https://handsontable.com/docs/scripts/json/save.json', {
            method: 'POST',
            mode: 'no-cors',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({data: change})
        })
        .then(Response => {
            exampleConsole.innerText = `Autosaved (${change.length} cell${change.length > 1 ? 's' : ''})`;
            console.log('The POST request is only used here for the demo purposes');
        });
    }
});

load.addEventListener('click', () => {
    fetch('https://handsontable.com/docs/scripts/json/load.json')
    .then (Response => {
        Response.json().then(data => {
            hot.loadData(data.data);
            exampleConsole.innerText = 'Data loaded';
        });
    });
});

save.addEventListener('click', () => {
    fetch('https://handsontable.com/docs/scripts/json/save.json', {
        method: 'POST',
        mode: 'no-cors',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({data: hot.getData() })
    })
    .then (Response => {
        exampleConsole.innerText = 'Data saved',
        console.log('The POST request is only used here for the demo purposes')
    });
});

autosave.addEventListener('click', () => {
    if (autosave.checked) {
      exampleConsole.innerText = 'Changes will be autosaved';
    } else {
      exampleConsole.innerText ='Changes will not be autosaved';
    }
  });
